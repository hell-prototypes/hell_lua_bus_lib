// ======================================================================
// Hell Lua Bus ( Lua Bind) Lib
// 
// Copyright 2013 Hell-Prototypes
//
// http://code.google.com/p/hell-prototypes/
//
// This is free software, licensed under the terms of the GNU General
// Public License as published by the Free Software Foundation.
// ======================================================================
#ifndef _define_h
#define _define_h
//define
#define MODULE_TIMESTAMP __DATE__ " " __TIME__
#define MODULE_NAMESPACE "Hell Lua Bus"
#define MODULE_VERSION "2.0"
#define MODULE_COPYRIGHT "Copyright (c) 2012-2013 Hell Prototypes "

#define DEFAULT_VID                  0x6666
#define DEFAULT_PID                  0xF0F0

#define NO_ERR				0
#define ERR_OPEN			1
#define ERR_CLAIM			2
#define ERR_CLOSE			3
#define ERR_NO_OPEN			4
#define ERR_CTRL_MSG		5
#define ERR_PARAM			6
#define ERR_PARAM_NUM		7
#define ERR_PARAM_TYPE		8
#define ERR_PARAM_VALUE		9
#define ERR_BULK_READ       10

#define PIN_NUM         	13
//#define EXT_PIN_INDEX_BASE	8

#define USB_PORT_WR			0xC0
#define USB_PORT_RD			0xC1
#define USB_OE_SET			0xC2

//#define USB_PINS_SET		0xC3
//#define USB_PINS_CLR		0xC4

#define USB_RS232_OPEN		0xD0
#define USB_RS232_RW		0xD1
#define USB_RS232_CLOSE		0xD2

#define USB_FW_VER			0xD3
#define USB_12V_CTRL		0xD4

#define USB_I2C_Start		0xD5
#define USB_I2C_Stop		0xD6
#define USB_I2C_Write		0xD7
#define USB_I2C_Read		0xD8

#define USB_SPI_INIT		0xD9
#define USB_SPI_PLAY_1BYTE	0xDA

#define USB_ICSP_RD			0xDB
#define USB_ICSP_WR			0xDC

#define USB_PDI_CTRL		0xDD
#define USB_PDI_RD			0xDE
#define USB_PDI_WR			0xDF
//==============================================================================
//For AES
#ifndef uint8
#define uint8  unsigned char
#endif

#ifndef uint32
#define uint32 unsigned long int
#endif

typedef struct aes_context_t
{
    uint32 erk[64];     /* encryption round keys */
    uint32 drk[64];     /* decryption round keys */
    int nr;             /* number of rounds */
} aes_context;

int  aes_set_key( aes_context *ctx, uint8 *key, int nbits );
void aes_encrypt( aes_context *ctx, uint8 input[16], uint8 output[16] );
void aes_decrypt( aes_context *ctx, uint8 input[16], uint8 output[16] );

#endif