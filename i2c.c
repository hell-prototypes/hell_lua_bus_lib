// ======================================================================
// Hell Lua Bus ( Lua Bind) Lib
// 
// Copyright 2013 Hell-Prototypes
//
// http://code.google.com/p/hell-prototypes/
//
// This is free software, licensed under the terms of the GNU General
// Public License as published by the Free Software Foundation.
// ======================================================================

#include <usb.h> /* libusb header */
//#include <unistd.h> /* for geteuid */
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include "define.h"

static int l_i2c_start(lua_State *L)
{
    lua_pop(L, lua_gettop(L));//clear stack

    if(g_usb_dev != NULL) {
        if( usb_control_msg(g_usb_dev, 0x40, USB_I2C_Start, 0x0000, 0x0000,
                            NULL, 0, 100) < 0) {
            lua_pushnumber (L, -ERR_CTRL_MSG);
        } else {
            lua_pushnumber (L, NO_ERR);
        }
    } else {
        lua_pushnumber (L, -ERR_NO_OPEN);
    }
    return 1;
}
static int l_i2c_stop(lua_State *L)
{
	lua_pop(L, lua_gettop(L));//clear stack

    if(g_usb_dev != NULL) {
        if( usb_control_msg(g_usb_dev, 0x40, USB_I2C_Stop, 0x0000, 0x0000,
                            NULL, 0, 100) < 0) {
            lua_pushnumber (L, -ERR_CTRL_MSG);
        } else {
            lua_pushnumber (L, NO_ERR);
        }
    } else {
        lua_pushnumber (L, -ERR_NO_OPEN);
    }
    return 1;
}

static int l_i2c_write(lua_State *L)
{

	unsigned char buffer[8];

	int value = get_int_value(L);

    if(value < 0) {
        lua_pushnumber (L, value);
        return 1;
    }

    if(g_usb_dev != NULL) {
        if(	usb_control_msg(g_usb_dev, 0x40 | 0x80, USB_I2C_Write, value, 0x0000,
                                (char *)buffer, 1, 1000 ) < 0) {
            lua_pushnumber (L, -ERR_CTRL_MSG);
        } else {
            lua_pushnumber (L, buffer[0] ? 1 : 0);
        }
    } else {
        lua_pushnumber (L, -ERR_NO_OPEN);
    }

    return 1;
}
static int l_i2c_read(lua_State *L)
{
	unsigned char buffer[8];

    int ack = get_int_value(L);

    if(ack < 0) {
        lua_pushnumber (L, ack);
        return 1;
    }
    if(ack) {
		ack = 1;
	}

    if(g_usb_dev != NULL) {
        if(	usb_control_msg(g_usb_dev, 0x40 | 0x80, USB_I2C_Read, ack, 0x0000,
                                (char *)buffer, 1, 1000 ) < 0) {
            lua_pushnumber (L, -ERR_CTRL_MSG);
        } else {
            lua_pushnumber (L, buffer[0]);
        }
    } else {
        lua_pushnumber (L, -ERR_NO_OPEN);
    }

    return 1;
}